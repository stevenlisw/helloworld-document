# LINUX安装TOMCAT
## 安装手册
### 获取TOMCAT
> 执行

```
wget http://mirrors.hust.edu.cn/apache/tomcat/tomcat-8/v8.0.42/bin/apache-tomcat-8.0.42.tar.gz
```
> 如果得到以下信息则成功下载

```
[root@localhost mydata]# wget http://mirrors.hust.edu.cn/apache/tomcat/tomcat-8/v8.0.42/bin/apache-tomcat-8.0.42.tar.gz
--2017-03-31 11:18:17--  http://mirrors.hust.edu.cn/apache/tomcat/tomcat-8/v8.0.42/bin/apache-tomcat-8.0.42.tar.gz
Resolving mirrors.hust.edu.cn... 202.114.18.160
Connecting to mirrors.hust.edu.cn|202.114.18.160|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9315107 (8.9M) [application/octet-stream]
Saving to: “apache-tomcat-8.0.42.tar.gz”
100%[=============================================================================================================================================================================>] 9,315,107   95.5K/s   in 65s     
2017-03-31 11:19:22 (139 KB/s) - “apache-tomcat-8.0.42.tar.gz” saved [9315107/9315107]
```

---

### 安装TOMCAT
> 进入mydata目录 【默认mydata是我们应用安装目录】
> 执行解压

```
tar -zxvf apache-tomcat-8.0.42.tar.gz
```

---

### 优化部分参数

- **优化启动参数**
> 进入tomcat/bin目录

```
vi catalina.sh
```
> 在cygwin=false上面加入以下内容后，保存退出

```
JAVA_OPTS="-Xms3096m -Xmx3096m -Xss1024K -XX:PermSize=128m -XX:MaxPermSize=1024m"
```

- **优化服务器配置**
> 进入tomcat/conf目录

```
vi server.xml
```
> 将Connector替换成以下内容后，保存退出

```
<Connector executor="tomcatThreadPool"
           port="8080" protocol="HTTP/1.1"
           minSpareThreads="25" maxSpareThreads="75"
      enableLookups="false" disableUploadTimeout="true" connectionTimeout="20000"
      acceptCount="300"  maxThreads="300" maxProcessors="1000" minProcessors="5"
      useURIValidationHack="false"
      compression="on" compressionMinSize="2048"
      compressableMimeType="text/html,text/xml,text/javascript,text/css,text/plain"
           redirectPort="8443"  URIEncoding="utf-8" />
```

---

### 启动TOMCAT
> 进入tomcat/bin目录

```
sh ./startup.sh
```
### 停止TOMCAT
> 进入tomcat/bin目录

```
sh ./shutdown.sh
```
**有时因为有线程正在执行，会有假停的现象，需要执行完shutdown后，再执行**
```
ps -ef|grep java
```
> 查看是否还有java进程在执行，如果有，可使用kill命令杀掉进程

```
kill -9 进程号
```
##END

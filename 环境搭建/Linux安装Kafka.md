```shell
#下载地址
https://kafka.apache.org/downloads
#解压
tar -zxvf kafka_2.13-3.3.1.tgz
#修改配置文件/config/server.properties
listeners=PLAINTEXT://内网IP:9092
advertised.listeners=PLAINTEXT://外网IP:9092
log.dirs=/mydata/kafka_2.13-3.3.1/kafka-logs
#启动
cd /mydata/kafka_2.13-3.3.1/
nohup ./bin/kafka-server-start.sh config/server.properties &

#创建topic
./bin/kafka-topics.sh --create --bootstrap-server 172.17.236.230:9092  --replication-factor 1 --partitions 1 --topic topicname
# 查看topic列表
./kafka-topics.sh --bootstrap-server 172.17.236.230:9092 --list
```

###集群配置
- 修改配置文件(config/server.properties)
```shell
#实例ID，每台机器不一样
broker.id=1
#本机IP地址
listeners=PLAINTEXT://172.21.238.0:9092
#使用IP作为hostname
host.name=172.21.238.0
#不需要外网访问，可以设置成内网，需要外网，此处使用外网IP
advertised.listeners=PLAINTEXT://172.21.238.0:9092
num.network.threads=12
num.io.threads=24
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
log.dirs=/opt/kafka_2.13-2.8.1/logs
num.partitions=1
num.recovery.threads.per.data.dir=12
offsets.topic.replication.factor=3
transaction.state.log.replication.factor=3
transaction.state.log.min.isr=3
cleanup.policy=delete
log.retention.hours=48
log.segment.bytes=1073741824
log.retention.check.interval.ms=300000
#zookeeper集群列表
zookeeper.connect=172.21.238.0:2181,172.21.238.2:2181,172.21.237.255:2181
zookeeper.connection.timeout.ms=18000
group.initial.rebalance.delay.ms=0
delete.topic.enable=true
```


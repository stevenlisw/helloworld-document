# linux安装mongo
## 脚本内容
### 1、创建mongodb.repo文件
```
vim /etc/yum.repos.d/mongodb-org-3.6.repo
```
在/etc/yum.repos.d/目录下创建文件mongodb.repo，它包含MongoDB仓库的配置信息，内容如下：
```
[mongodb]
name=MongoDB Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
gpgcheck=0
enabled=1
```
### 2、执行安装命令
```
yum install mongodb-org
```
### 3、创建用户
```
mongo --port 27017
use admin
db.createUser({user:"userAdmin",pwd:"123456",roles:[{role:"userAdminAnyDatabase",db:"admin"}]})
db.auth("userAdmin","123456")
```
### 4、重启mongo服务
```
service mongod stop
service mongod start
```

>常规方式安装node.js一台电脑只能安装一个，例如之前项目安装的node.js版本是v12，
> 而我们现在的新项目需要依赖node.js---->v13，此时安装v13后，
> 之前的v12将被覆盖，而且之前的项目不支持v12的运行，
> 我们如果想运行之前的项目就需要卸载高版本的v13，重新下载v12版本十分麻烦！ 
> 所以就有了nvm ：node管理工具。

> 总的来说就是可以在电脑上随意切换多个版本的nodejs，更方便的兼容我们的不同node版本的项目程序
 


## 下载NVM for Windows

获取地址：https://github.com/coreybutler/nvm-windows/releases

[![image65aa7c2aeeab636c.png](https://cdnjson.com/images/2023/04/02/image65aa7c2aeeab636c.png)](https://cdnjson.com/image/j48QQ)

nvm-noinstall.zip： 这个是绿色免安装版本，但是使用之前需要配置

nvm-setup.zip：这是一个安装包，下载之后点击安装，无需配置就可以使用，方便。【推荐】

Source code(zip)：zip压缩的源码

Sourc code(tar.gz)：tar.gz的源码，一般用于*nix系统


## 安装NVM for Windows

解压文件后双击  nvm-setup.exe  安装【傻瓜式安装】

## 测试
安装好以后，打开命令行，输入 ```nvm version``` 查看版本，显示版本号即为安装成功

## 常用命令
```
nvm ls available  // 显示所有可以下载的版本
nvm ls // 查看目前已经安装的版本
nvm install 14.18.0  // 安装指定的版本的node.js
nvm install 10.15.3
nvm use 14.18.0  // 使用指定版本的node.js
nvm use 10.15.3  //两个版本随意切换
nvm ls-remote ：列出所有远程服务器的版本（官方node version list）

nvm list ：列出所有已安装的 node 版本

nvm install latest ：安装最新版 node

nvm install [node版本号] ：安装指定版本 node

nvm uninstall [node版本号] ：删除已安装的指定版本

nvm use [node版本号] ：切换到指定版本 node

nvm current ：当前 node 版本

nvm alias [别名] [node版本号] ：给不同的版本号添加别名

nvm unalias [别名] ：删除已定义的别名

nvm alias default [node版本号] ：设置默认版本
```
nvm在安装node时由于是外网Github所以下载特别慢，别着急，可以在nvm中找到settings.txt文件末尾添加
```
node_mirror: https://npm.taobao.org/mirrors/node/
npm_mirror: https://npm.taobao.org/mirrors/npm/
```
>注：settings.txt在nvm安装目录中，具体以自己安装时选择目录为准。
![image847039423b58688f.png](https://cdnjson.com/images/2023/04/02/image847039423b58688f.png)


## 相关案例说明

### 安装指定nodejs版本

```shell
# 安装node12.22.7
C:\Users\Administrator>nvm install 12.22.7
Downloading node.js version 12.22.7 (64-bit)...
Complete
Creating C:\Users\Administrator\AppData\Roaming\nvm\temp

Downloading npm version 6.14.15... Complete
Installing npm v6.14.15...

Installation complete. If you want to use this version, type
```
### 查看目前有什么版本
```shell
C:\Users\Administrator>nvm list

    19.8.1
    18.14.2
    12.22.7
```
### 查看当前使用的什么版本
```shell
C:\Users\Administrator>nvm current
v19.8.1
```
### 切换某一版本
```shell
C:\Users\Administrator>nvm use 18.14.2
C:\Users\Administrator>nvm use 18.14.2
Now using node v18.14.2 (64-bit)
```

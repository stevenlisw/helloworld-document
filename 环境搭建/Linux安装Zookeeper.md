```shell
#下载地址
http://archive.apache.org/dist/zookeeper/
#解压
tar -zxvf apache-zookeeper-3.6.2-bin.tar.gz
#生成配置文件
[root@iZk1a9zlh3yyagozfen35uZ mydata]# cd apache-zookeeper-3.7.0-bin/
[root@iZk1a9zlh3yyagozfen35uZ apache-zookeeper-3.7.0-bin]# cd conf/
[root@iZk1a9zlh3yyagozfen35uZ conf]# cp zoo_sample.cfg zoo.cfg
#修改配置文件
dataDir=/mydata/apache-zookeeper-3.7.0-bin/data
dataLogDir=/mydata/apache-zookeeper-3.7.0-bin/logs
#启动
[root@iZk1a9zlh3yyagozfen35uZ conf]# cd ../bin
[root@iZk1a9zlh3yyagozfen35uZ bin]# ./zkServer.sh start
```

###集群配置
- 修改配置文件(config/zoo.cfg)
```shell
# The number of milliseconds of each tick
tickTime=2000
# The number of ticks that the initial 
# synchronization phase can take
initLimit=10
# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
syncLimit=5
# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
dataDir=/opt/apache-zookeeper-3.5.9/data
dataLogDir=/opt/apache-zookeeper-3.5.9/logs
# the port at which the clients will connect
clientPort=2181
server.1=172.21.238.0:2888:3888
server.2=172.21.238.2:2888:3888
server.3=172.21.237.255:2888:3888
# the maximum number of client connections.
# increase this if you need to handle more clients
#maxClientCnxns=60
#
# Be sure to read the maintenance section of the 
# administrator guide before turning on autopurge.
#
# http://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance
#
# The number of snapshots to retain in dataDir
#autopurge.snapRetainCount=3
# Purge task interval in hours
# Set to "0" to disable auto purge feature
#autopurge.purgeInterval=1
```
- 创建myid
在dataDir（具体看上述配置文件中的配置目录，此处是/opt/apache-zookeeper-3.5.9/data）中创建myid，需要集群的机器都要创建。
文件内容从1开始递增。例如机器1，内容是1，机器2，内容是2。
```shell
cd /opt/apache-zookeeper-3.5.9/data
#每台机器都要执行
touch myid
#每台机器都要执行，但数字需要修改。
echo 1 > /opt/apache-zookeeper-3.5.9/data/myid
```


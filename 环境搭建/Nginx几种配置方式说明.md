### 多域名指向不同项目路径配置方式
>当我们有多个项目部署在同一台机器，但是每个项目又有不同的域名。可以使用Nginx解决。配置如下：
```text
server {
    listen       80;
    server_name  aaa.com;

	location /{
    	    proxy_pass http://127.0.0.1:8890;
    }
    
}
server {
    listen       80;
    server_name  bbb.com;

    location /{
    	    proxy_pass http://127.0.0.1:8080;
    }
}
```
### 使用nginx负载均衡
>当我们后端服务部署了多套时，可以nginx进行做负载均衡。配置如下：
```text
upstream project{
	server 127.0.0.1:8080 weight=1;
	server 172.21.237.251:8080 weight=1;
	server 172.21.237.249:8080 weight=1;
}
server {
    listen       80;
    server_name  aaa.com;

    location /{
    	    proxy_pass http://project;
    }
}
#其中weight代表权重，此处设置相同，权重一样。
```

### nginx-Https证书配置
从第三方购买相关证书后，拿到nginx的crt及key两个证书文件。放到服务器某个目录。调整nginx配置文件，如下:

其中aaaa.com为域名，/opt/aaaa.crt与/opt/aaaa.key为证书文件。
```text
  server {
        listen 443;
        server_name aaaa.com; #域名
        ssl off; 
        ssl_certificate /opt/aaaa.crt; #证书文件目录
        ssl_certificate_key /opt/aaaa.key; #证书文件目录
        ssl_session_timeout 40m;
        ssl_protocols  TLSv1.1 TLSv1.2; 
        ssl_ciphers ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES128-SHA256:DHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4;
        ssl_prefer_server_ciphers on;
  		add_header X-Frame-Options SAMEORIGIN;
  		add_header X-Content-Type-Options nosniff;
    }
```

### nginx路径转发
当我们需要进行匹配路径，去转发到后端服务时，可以使用nginx的转发配置。

-目录地址最后不带/，则会将匹配路径带入到目录地址中
```text
#访问http://xxxx/nacos,会转发到http://127.0.0.1:8848/nacos
location /nacos {
        proxy_pass http://127.0.0.1:8848;
    }
```
-目录地址带/,则不会将匹配路径带入到目录地址中
```text
#访问http://xxxx/nacos,会转发到http://127.0.0.1:8848
location /nacos {
        proxy_pass http://127.0.0.1:8848/;
    }
```


# 利用Yum安装MySQL
## 安装手册
### 获取MySQL

>在mysql官网下载对应的rpm包
> 
>https://dev.mysql.com/downloads/repo/yum/ 

![image.png](https://cdnjson.com/images/2023/04/04/image.png)

![imagedc1fa440784d416b.png](https://cdnjson.com/images/2023/04/04/imagedc1fa440784d416b.png)
> 
>下载后，放到服务器

```shell
rpm -ivh mysql80-community-release-el8-4.noarch.rpm 
yum clean all 
yum makecache 
yum repolist | grep mysql 
yum install -y mysql-community-server
#如果报，没有任何匹配: mysql-community-server
[root@hecs-279107 mydata]# yum install -y mysql-community-server
Failed to set locale, defaulting to C.UTF-8
Last metadata expiration check: 0:46:59 ago on Tue Apr  4 13:47:40 2023.
All matches were filtered out by modular filtering for argument: mysql-community-server
Error: Unable to find a match: mysql-community-server

#则先禁用默认的mysql模块
sudo yum module disable mysql
#在执行
yum install -y mysql-community-server

#设置相关配置/etc/my.cnf
#设置不区分大小写(其他参数，按需配置)
#lower_case_table_names=1
systemctl start mysqld

#mysql8，初始化以后修改配置文件里面的lower_case_table_names=1会无法启动，因为这个参数要在
#初始化的时候就要设置，yum安装无法初始化，可以用过删除/var/lib/mysql后，重新启动即可，重新
#启动后，会再次初始化。
systemctl stop mysqld
rm -rf /var/lib/mysql
systemctl restart mysqld
#密码在/var/log/mysqld.log
mysql -u root -p 
#重新设置密码
ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';
#更改root账号的host为 %
use mysql;
#允许远程登录
update user set host='%' where user='root';
flush privileges;
```

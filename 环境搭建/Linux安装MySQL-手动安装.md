# 利用tar手动安装MySQL
## 安装手册
### 获取MySQL

>在mysql官网下载对应的rpm包
> 
>https://dev.mysql.com/downloads/mysql/ 
> 
>下载后，放到服务器

### 安装
- 解压下载好的安装包

```shell
tar xvf mysql-8.0.29-linux-glibc2.12-x86_64.tar.xz
```
- 重命名解压出来的文件夹，移动到目标目录，这里改成mysql
```shell
mv ./mysql-8.0.29-linux-glibc2.12-x86_64  /usr/local/mysql
```
- /usr/local/mysql文件夹下创建data文件夹 存储文件
```shell
cd  /usr/local/mysql
mkdir data
```
- 分别创建用户组以及用户和密码（如果提示已存在说明之前有创建过了）
```shell
groupadd mysql
useradd -g mysql mysql
```
- 授权刚刚新建的用户
```shell
chown -R mysql.mysql /usr/local/mysql
chmod 750 /usr/local/mysql/data -R
```
- 配置环境变量
```shell
vim /etc/profile
#加入环境变量
export PATH=$PATH:/usr/local/mysql/bin:/usr/local/mysql/lib
```
```shell
source /etc/profile
```
- 配置文件修改,编辑my.cnf文件,内容如下：
```shell
[mysql]
default-character-set=utf8mb4
[client]
#port=3306
socket=/var/lib/mysql/mysql.sock
 
[mysqld]
#port=3306
#server-id=3306
user=mysql
general_log = 1
general_log_file= /var/log/mysql/mysql.log
socket=/var/lib/mysql/mysql.sock
basedir=/usr/local/mysql
datadir=/usr/local/mysql/data
log-bin=/usr/local/mysql/data/mysql-bin
innodb_data_home_dir=/usr/local/mysql/data
innodb_log_group_home_dir=/usr/local/mysql/data/
character-set-server=utf8mb4
lower_case_table_names=1
autocommit=1
default_authentication_plugin=mysql_native_password
symbolic-links=0
# Disabling symbolic-links is recommended to prevent assorted security risks
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd
 
[mysqld_safe]
log-error=/usr/local/mysql/data/mysql.log
pid-file=/usr/local/mysql/data/mysql.pid
```
- 初始化基础信息,得到数据库的初始密码（在/usr/local/mysql/bin目录下执行）
```shell
cd /usr/local/mysql/bin
./mysqld --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data/ --initialize
#执行上述命令后，会打印初始的MySQL密码，如下内容:
# A temporary password is generated for root@localhost:<*FRw4ifwRt
# 其中的<*FRw4ifwRt便是初始密码
```
- 复制 mysql.server 文件，在/usr/local/mysql目录下执行
```shell
cd /usr/local/mysql
cp  ./support-files/mysql.server /etc/init.d/mysql
cp  ./support-files/mysql.server /etc/init.d/mysqld
```
- 赋予权限
```shell
chown 777 /etc/my.cnf
chmod +x /etc/init.d/mysql
chmod +x /etc/init.d/mysqld
```
- 检查一下/var/lib/mysql是否存在，否则进行创建
```shell
mkdir /var/lib/mysql 
chown -R mysql:mysql /var/lib/mysql/ 
```
- 启动数据库，有SUCCESS字眼说明MySQL安装完成
```shell
service mysql start 
```
- 日志查看(mysql.log)

### 登陆MySQL
```shell
mysql -uroot -pns#WvaqE,3#j   （上面生成的密码）
#登录mysql出错：mysql: error while loading shared libraries: libtinfo.so.5: cannot open share
#因为安装的是glibc版本，所以可能会缺少依赖，
#安装：yum install  libncurses.so.5
```
- 修改密码，并设置远程连接
```shell
#修改密码
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '新密码';
#使密码生效
flush privileges;
use mysql;
#修改远程连接并生效
update user set host='%' where user='root';
flush privileges;
```
>如果外网无法链接，检查防火墙，如果使云服务器，则需要在云服务器后台开放3306端口
###END


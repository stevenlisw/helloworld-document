
## 下载prometheus

https://prometheus.io/download/

![1](../images/img_4.png)


## 将prometheus设置成启动服务

### 进入systemd目录
```shell
cd /usr/lib/systemd/system
```

### 创建启动服务配置文件

```shell
vim prometheus.service
```
内容如下:
```text
[Unit]
Description=https://prometheus.io
[Service]
Restart=on-failure
ExecStart=/usr/local/prometheus/prometheus --config.file=/usr/local/prometheus/prometheus.yml --storage.tsdb.path=/usr/local/prometheus/data
[Install]                      
WantedBy=multi-user.target
```
注意:
以上ExecStart中分别指定了prometheus的启动文件、配置文件、数据存储的三个磁盘地址。具体按照你的prometheus放在哪里了去指定

### 生效系统systemd文件

```shell
systemctl daemon-reload
```

### 启动和停止服务命令

```shell
# 启动
systemctl start prometheus.service
# 停止
systemctl stop prometheus.service
```

## 下载grafana

https://grafana.com/get/?plcmt=top-nav&cta=downloads

![1](../images/img_4.png)

![1](../images/img_6.png)
上述下载的具体安装包，以自己操作系统去选择，此处使用centos为例

```shell
#1.由于grafana新安装，需要加载新的配置文件使其生效，之后启动就不需要了
sudo systemctl daemon-reload

#2.启动grafana服务器
sudo systemctl start grafana-server

#3.检查服务器状态
sudo systemctl status grafana-server
```

访问：http://xxxxxx:3000

## 配置Linux服务器的指标监控

### 下载Linux监控的的exporter

#### 被监控端配置node_exporter

下载被监控端软件地址：https://github.com/prometheus/node_exporter/releases/tag/v1.4.0
下载后放入服务器，解压执行一下命令启动
```shell
nohup ./node_exporter &
```
测试是否有数据
http://x.x.x.x:9100/metrics

将 node_exporter客户端加入prometheus.yml配置中
scrape_configs下增加:

- job_name: "linux"
  static_configs:
  - targets: ["127.0.0.1:9100"] 

重启prometheus
```shell
systemctl restart prometheus.service
```

## 在grafana中配置

![1](../images/img_7.png)
![1](../images/img_8.png)
输入11074与9276都可以，这两个都是监控linux的配置。其他id号可以通过官网获取https://grafana.com/grafana/dashboards
每一个不同的id号对应着不同的表盘。
![1](../images/img_9.png)
![1](../images/img_10.png)

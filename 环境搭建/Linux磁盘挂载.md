# Linux磁盘挂载

### 查看磁盘情况
```shell
#查看是否有数据盘需要挂载
fdisk -l
```
###对/dev/vdb进行分区
```shell
fdisk /dev/vdb
#根据提示依次输入n,p,1,回车,回车
```
###磁盘格式化
```shell
#有数据的磁盘，根据实际情况决定是否要格式化
mkfs.ext4 /dev/vdb1
```

###挂载到目录

- 挂载到新目录，如果目录中有需要保留的数据，则忽略本步，往下看。
```shell
mount /dev/vdb1 /opt/
```
- 挂载到老目录（目录中有数据）
```shell
#目录已经有数据，需要做跳板操作，否则数据将会丢失
#创建一个用于跳板的目录
mkdir /new
#先挂载到跳板目录
mount /dev/vdb1 /new
#将文件复制到跳板目录
cp -R /opt/* /new/
#删除原有目录中的文件
rm -rf /opt/*
#挂载到我们需要的目录
mount /dev/vdb1 /opt
#此时你会发现之前的/opt目录文件已经全部转移过来了,
#也许你会有点疑问,我并没有复制或移动/new文件到新挂载的/opt目录啊,
#其实此时的/opt目录相当于/new目录的硬链接,
#可以测试下mkdir /new/test 你会发现/opt目录也存在test
#解除挂载的跳板目录
umount /new
#删除跳板目录
rm -rf /new 
#查看是否挂载成功
df -h
```

###开启重启，自动挂载
```shell
#重启开机自动挂载
blkid #查看磁盘UUID
#修改/etc/fstab文件，追加如下：
UUID=05927a5e-cb18-4372-a813-ee9101b26b0a /opt                       ext3    defaults        0 0
#启动UUID换成blkid对应的磁盘ID
```


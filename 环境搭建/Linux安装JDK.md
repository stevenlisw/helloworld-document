# 利用Yum快速安装OPENJDK
```shell
#快捷安装jdk
yum search java|grep jdk
yum install java-1.8.0-openjdk
#验证 
java -version
```
# 手动安装JDK
## 下载JDK
- 从oracle官网下载
https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html

- 从阿里云盘下载
https://www.aliyundrive.com/s/tNNV5rttdbD

##将安装包上传到服务器
```
rpm -ivh jdk-8u45-linux-x64.rpm
```

---

### 检验安装是否成功
```
java -version
```

---

### JAVA版本切换
```
[root@localhost mydata]# java -version
java version "1.7.0_45"
OpenJDK Runtime Environment (rhel-2.4.3.3.el6-x86_64 u45-b15)
OpenJDK 64-Bit Server VM (build 24.45-b08, mixed mode)
```
如果出现类似信息，是**OpenJDK**,则需要切换JAVA版本
> 输入

```
update-alternatives --config java
```
> 选择相应的序号

```
There are 4 programs which provide 'java'.
  Selection    Command
-----------------------------------------------
*+ 1           /usr/lib/jvm/jre-1.7.0-openjdk.x86_64/bin/java
   2           /usr/lib/jvm/jre-1.5.0-gcj/bin/java
   3           /usr/lib/jvm/jre-1.6.0-openjdk.x86_64/bin/java
   4           /usr/java/jdk1.8.0_45/jre/bin/java
Enter to keep the current selection[+], or type selection number: 4
```
再输入
```
[root@localhost mydata]# java -version
java version "1.8.0_45"
Java(TM) SE Runtime Environment (build 1.8.0_45-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.45-b02, mixed mode)
```
这样就可正常使用了

---

### 设置环境变量
> 这里我们需要找到profile文件，添加环境变量：

```
vim /etc/profile
```
> 在profile文件下面追加写入下面信息：

```
export JAVA_HOME=/usr/java/jdk1.8.0_45
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
```
> 保存退出后执行:

```
source /etc/profile
```
### END



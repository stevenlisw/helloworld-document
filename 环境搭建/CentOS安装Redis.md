# LINUX安装Redis
## 安装手册
### 获取REDIS
```
wget http://download.redis.io/releases/redis-6.2.7.tar.gz
```
---

### 安装REDIS

- 解压、进入、编译
```
tar xzf redis-6.2.7.tar.gz
cd redis-6.2.7
make
```
- 安装
```shell
cd src
make install
```

### 启动
```shell
./redis-server ../redis.conf
```

### 修改配置文件（修改redis.conf文件）

- 允许远程链接

> 注释bind 127.0.0.1这一行

- 修改保护模式
>protected-mode yes 改成 protected-mode no,如下
>protected-mode no
- 后台启动
>daemonize no 改为yes 后台一直运行，如下
> daemonize yes
- 设置密码
>requirepass "你的密码"




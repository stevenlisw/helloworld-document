
### 安装navicat

官网下载地址：https://www.navicat.com.cn/download/navicat-premium

![image5962067dbd4039b4.png](https://cdnjson.com/images/2023/04/05/image5962067dbd4039b4.png)

然后双击下一步下一步,选择自己的安装路径即可.

![imaged8ad9128f642e934.png](https://cdnjson.com/images/2023/04/05/imaged8ad9128f642e934.png)

### 激活
这里的激活方式又两种,一种是无限试用版,一种就是永久激活.

**激活文件加我微信获取吧**

无限试用的话是支持在线更新的.

永久激活是无法在线跟新的,除非卸载重装最新版才可以.

就看个人喜好啦!

#### 1.无限试用版
如图所示,任选一个脚本运行即可.

![imagef0d211c0f400b460.png](https://cdnjson.com/images/2023/04/05/imagef0d211c0f400b460.png)

注意: 如果使用脚本一提示找不到指定的注册表,请选用脚本2即可.
#### 2.永久激活版

点击运行NavicatCracker.exe

![image033ca69113053e20.png](https://cdnjson.com/images/2023/04/05/image033ca69113053e20.png)

选择Navicat安装目录.

![image59823a9fcdb5ba94.png](https://cdnjson.com/images/2023/04/05/image59823a9fcdb5ba94.png)

有的小伙伴就需要断网才能成功；如果联网失败了再断网尝试一下!!!

点击 Patch!

![imageb12688bee1b0329f.png](https://cdnjson.com/images/2023/04/05/imageb12688bee1b0329f.png)

然后点击是,请勿选择否,否则重装再来一次

![imagefceb35ac5fae49ac.png](https://cdnjson.com/images/2023/04/05/imagefceb35ac5fae49ac.png)

然后打开Navicat Premium 16，可以直接进行注册；

如果点击了试用可以进去点击帮助-->注册；如图：

![imagedf1deb7a176890db.png](https://cdnjson.com/images/2023/04/05/imagedf1deb7a176890db.png)

打开注册软件.点击 Generate生成注册码.再点击copy.

![ima![img.png](img.png)ge76aedcecac7c0a6a.png](https://cdnjson.com/images/2023/04/05/image76aedcecac7c0a6a.png)

粘贴刚刚生成的注册码,点击激活.

选择手动激活.

[![pp5OXbd.png](https://s1.ax1x.com/2023/04/05/pp5OXbd.png)](https://imgse.com/i/pp5OXbd)

复制生成的请求码.
[![pp5OvVA.png](https://s1.ax1x.com/2023/04/05/pp5OvVA.png)](https://imgse.com/i/pp5OvVA)

将 Navicat 给的请求码复制到注册机 Request Code 中
[![pp5XpPP.png](https://s1.ax1x.com/2023/04/05/pp5XpPP.png)](https://imgse.com/i/pp5XpPP)

点击 Generate Activation Code进行激活.复制激活码.
[![pp5XC28.png](https://s1.ax1x.com/2023/04/05/pp5XC28.png)](https://imgse.com/i/pp5XC28)

然后粘贴激活码.点击激活即可.

[![pp5XFKg.png](https://s1.ax1x.com/2023/04/05/pp5XFKg.png)](https://imgse.com/i/pp5XFKg)
然后就激活成功啦!完美!
[![pp5XVVs.png](https://s1.ax1x.com/2023/04/05/pp5XVVs.png)](https://imgse.com/i/pp5XVVs)

